var express=require('express');
var app=express();
var http=require('http').Server(app);
var io=require('socket.io')(http);

var crypto=require('crypto');




app.get('/',function(req,res){
	res.send('<h1> Hello World</h1>');
});

http.listen(3002,function(){
	console.log("Servidor corriendo en http://localhost:3002");
});

io.on('connection', function(socket) {  
  console.log('Alguien se ha conectado con Sockets');
  
  //socket.emit('messages', messages);

  socket.on('PaqueteCentral', function(data) {

    console.log("---------------------------------------------------");

    var stringJSON=data.ecg+""+data.spo+""+data.tar+""+data.tmp;
    var checkValidado=checksum(stringJSON);

    if(data.checksum==checkValidado)
    	console.log(data.numero);
    //io.sockets.emit('messages', messages);

  });

  socket.on('disconnect',function(){
      console.log("El cliente: se ha DESCONECTADO!!");

      socket.leave(socket.room);
  });

});

io.on('reconnect',function(cliente){
    console.log("Cliente Reconectado...");
});

/*
io.sockets.on('connection',function(socket){
	console.log("Se ha conectado un navegador");
});
*/

function checksum(string,algoritm,endcoding){
	return crypto.createHash(algoritm || 'md5')
			.update(string,'utf8')
			.digest(endcoding || 'hex')
}